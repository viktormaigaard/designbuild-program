//
//  main.c
//  DesignBuild
//
//  Created by Viktor Poulsen on 26/11/14.
//  Copyright (c) 2014 Viktor Poulsen. All rights reserved.
//

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <time.h>

#define tofind    "\ +\+"

typedef struct
{
    int measure_ID;
    
    float current;
    double time;
    double temperature;
    float voltage;
    
    float effect;
    
}measurement;

void menu();
int read_file();
void print_data();
void calculate_effect();

int main(int argc, const char * argv[]) {
    int measure_count = 0;
    measurement measurements[50000];
    
    menu(measurements, measure_count);

    return 0;
}

void menu(measurement measurements[], int measure_count)
{
    
    char repeater = 'y';
    int Rth = 0;
    int Cth = 0;
    
    /* Hovedmenu */
    do
    {
        int choice, choice2;
        printf("################################\n");
        printf("Chose function\n");
        printf("1. Read file\n");
        printf("2. Calculations\n");
        printf("3. Print Data\n");
        printf("4. Exit\n");
        scanf("%i", &choice);
        printf("################################\n");
        
        switch(choice)
        {
            //Read File
            case 1:
                measure_count = read_file(measurements, measure_count);
                break;
            //Do Calculationes
            case 2:
                printf("################################\n");
                printf("Chose function\n");
                printf("1. Calculate electrical effect for each measurement.\n");
                printf("2. Calculate theoretical temperature for each measurement.\n");
                printf("3. Calculate theoretical temperature for other Rth, Cth\n");
                scanf("%i", &choice2);
                printf("################################\n");
                //Sub Menu for calculations
                switch(choice2)
                {
                    //Calculate Effect
                    case 1:
                        calculate_effect(measurements, measure_count);
                        break;
                    //Calculate temperature
                    case 2:
                        break;
                    case 3:
                        break;
                    default:
                        break;
                }
                break;
            //Print out measurements
            case 3:
                print_data(measurements, measure_count);
                break;
            //End Program
            case 4:
                repeater = 'n';
                break;
            default:
                break;
        }
    }
    while(repeater != 'n');
}

int read_file(measurement measurements[], int measure_count){
    FILE * fp;
    size_t len = 0;
    ssize_t read;
    char * line = NULL, file_name[20], dtm[100];
    int old_measurements_count = measure_count;;
    double time_spent;
    
    //Timer Starts
    clock_t begin, end;
    begin = clock();
    
    //import temp and voltage
    strcpy(file_name, "temp_voltage.dat");
    fp = fopen(file_name , "r");
    float loop_count = 0, temp_current, temp_voltage;
    double temp_time, temp_temperature;
    char temp_time_str[50];
    while ( (read = getline(&line, &len, fp)) != -1)
    {
        loop_count ++;
        strcpy( dtm, line);
        if (loop_count>10)
        {
            sscanf(dtm, "%lf %lf %f", &temp_time, &temp_temperature, &temp_voltage);
            printf("%lf %f %f\n", temp_time, temp_temperature, temp_voltage);
            temp_time = floor(temp_time*10)/10;
            
            measurements[measure_count].measure_ID = measure_count;
            measurements[measure_count].voltage = temp_voltage;
            measurements[measure_count].time = temp_time;
            measurements[measure_count].temperature = temp_temperature;
            
            measure_count++;
        }
    }
    fclose(fp);
    //import temp and voltage
    strcpy(file_name, "current.dat");
    loop_count = 0;
    temp_current = 0;
    temp_voltage = 0;
    temp_temperature = 0;
    temp_time = 0;
    fp = fopen(file_name , "r");
    
    while ( (read = getline(&line, &len, fp)) != -1)
    {
        loop_count ++;
        strcpy( dtm, line);
        if (loop_count>10)
        {
            sscanf(dtm, "%lf %f", &temp_time, &temp_current);
            printf("%lf %f\n", temp_time, temp_current);
            temp_time = floor(temp_time*10)/10;
            for (int i = 0; i < measure_count; i++) {
                if (temp_time == measurements[i].time) {
                    measurements[i].current = temp_current/100;
                }
                else{
                    measurements[measure_count].measure_ID = measure_count;
                    measurements[measure_count].time = temp_time;
                    measurements[measure_count].current = temp_current/100;
                }
            }
            measure_count++;
        }
    }
    fclose(fp);
    
    if (line)
        free(line);
    
    //stop timer and print time spent + return measure count so we can keep counting upwards
    end = clock();
    time_spent = (double)(end - begin) / CLOCKS_PER_SEC;
    printf("\n%.2f seconds spent reading %d measurements for a total of %d measurements\n", time_spent, measure_count-old_measurements_count, measure_count);
    return measure_count;
}

void calculate_effect(measurement measurements[], int measure_count){
    for (int i = 0; i<measure_count; i++) {
        measurements[i].effect = measurements[i].current * measurements[i].voltage;
    }

    printf("\nCalculated effect for %d measurements\n", measure_count);
}

void print_data(measurement measurements[], int measure_count)
{
    for (int i = 0; i < measure_count; i++) {
        printf("\nMeasurement ID %d:\n", measurements[i].measure_ID);
        printf("   Time: %f\n", measurements[i].time);
        printf("   Current: %f\n", measurements[i].current);
        printf("   Voltage: %f\n", measurements[i].voltage);
        printf("   Effect: %f\n", measurements[i].effect);
        printf("   Temperature: %f\n", measurements[i].temperature);
    }
}
